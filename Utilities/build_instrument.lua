--[[--------------------------------------------------------------------------------------------------
-- 	 BSVH: Build Instrument Script
--]]--------------------------------------------------------------------------------------------------

-- Creator Tools built-ins

fs = filesystem

-- Check for valid instrument
if not instrument then
    print("Error: Creator Tools are not focused on a Kontakt instrument." ..
          "Load an instrument in Kontakt and ensure it is selected.")

    -- exit script here
    return 1
end

-- Set global filepaths; Change ROOT_DIR to absolute path of project root
ROOT_DIR = '/Users/coreybresnan/Development/b-side-horns-project'

ASSETS_DIR = ROOT_DIR .. '/Resources'
SAMPLES_DIR = ROOT_DIR .. '/Samples'
UTILS_DIR = ROOT_DIR .. '/Utilities'


if not fs.exists(ROOT_DIR) then
    print('Error: Root directory does not exist: ' .. ROOT_DIR ..
          '\nROOT_DIR constant must be set to project root.')

    -- exit script here
    return 1

elseif not fs.exists(ASSETS_DIR) then
    print('Error: Assets directory does not exist: ' .. ASSETS_DIR ..
          '\nCheck that ROOT_DIR is set to project root.')

    -- exit script here
    return 1

elseif not fs.exists(SAMPLES_DIR) then
    print('Error: Samples directory does not exist: ' .. SAMPLES_DIR ..
          '\nCheck that ROOT_DIR is set to project root.')

    -- exit script here
    return 1

elseif not fs.exists(UTILS_DIR) then
    print('Error: Utils directory does not exist: ' .. UTILS_DIR ..
          '\nCheck that ROOT_DIR is set to project root.')

    -- exit script here
    return 1
end

-- Constants declarations

-- Get prefix from temporary instrument name 
INST_PREFIX = instrument.name

INSTR_MAP = {
    PTYP = 'Prototype',
    TRBN = 'Trombone',
    BSAX = 'Baritone Sax',
    TSAX = 'Tenor Sax',
    TRPT = 'Trumpet'
}

-- Check that the instrument name in Kontakt is a valid prefix value
if INSTR_MAP[INST_PREFIX] == nil then
    print('Error: Instrument name must be set to a valid prefix in Kontakt: ' .. 
          'PTYP, TRBN, BSAX, TSAX, TRPT\n' ..
          'Current intrument name: ' .. instrument.name)

    -- exit script here
    return 1
end

SHARPS_MAP = {
    "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"
}

FLATS_MAP = {
    "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"
}

-- Element names can't use # symbol in lua
NOTE_MAP = {
    C = 0, Cs = 1, D = 2, Ds = 3, E = 4, F = 5,
    Fs = 6, G = 7, Gs = 8, A = 9, As = 10, B = 11,
    Db = 1, Eb = 3, Gb = 6, Ab = 8, Bb = 10
}

-- MIDI DYNAMICS STANDARD:
--     PPPP = 8, PPP = 20, PP = 31, P = 42, MP = 53,
--     MF = 64, F = 80, FF = 96, FFF = 112, FFFF = 127

DYNAMICS_MAP = {
    MF = { 0, 49 },
    MP = { 0, 49 }, -- only needed for PTYP
    F  = { 50, 79 },
    FF = { 80, 127 }
}

LEGATO_MAP = {
    MF = 14,
    MP = 14, -- only needed for PTYP
    F = 42,
    FF = 70
}


function midi_to_note( midi, flats )
	
	if flats then
		map = FLATS_MAP
	else
		map = SHARPS_MAP
	end

	-- Calculate the octave and note within the octave
    octave = (midi // 12) - 1
    interval = (midi % 12) + 1

    -- Get the note name
    note_name = map[interval] .. octave

    return note_name
end


function note_to_midi( note )
	
	-- Extract the note and octave from the note name
    interval = note:sub(0,-2)
    octave = tonumber(note:sub(-1))

    -- Check if this is a valid note name
    if NOTE_MAP[interval] == nil then
        print("note_to_midi: Invalid note name provided: " .. note )
        return nil
    end

    -- Calculate the MIDI note number
    midi = (octave + 1) * 12 + NOTE_MAP[interval]

    return midi
end

function import_sample( scg, ag, dyn, t1, t2, rnd, sfp )
    
    local gindex
    local glabel

    local zone = Zone()
    zone.file = sfp
    
    local root = note_to_midi(t1:gsub('#','s'))
    zone.rootKey = root
    zone.keyRange.low = root
    zone.keyRange.high = root + 1

    -- Legato uses velocity to define target matrix
    if ag == 'LEG' then
        
        local target = note_to_midi(t2:gsub('#','s'))
        local interval = target - root

        if target < root then
            zone.rootKey = target
            zone.keyRange.low = target
            zone.keyRange.high = target + 1
        end

        glabel = table.concat({ scg, ag, interval }, '_')
    else
        glabel = table.concat({ scg, ag, rnd }, '_')
    end

    zone.velocityRange.low = DYNAMICS_MAP[dyn][1]
    zone.velocityRange.high = DYNAMICS_MAP[dyn][2]

    -- check if the group already exists
    for i, g in pairs(instrument.groups) do
        if g.name == glabel then
            gindex = i
            break
        else
            gindex = nil
        end
    end

    if gindex then
        print('Inserting sample: ' .. sfp )
        instrument.groups[gindex].zones:add(zone)
    else
        print('Creating new group: ' .. glabel)
        local group = Group()
        group.name = glabel
        group.zones:add(zone)
        instrument.groups:add(group)
    end

end

-- Log instrument constants to console
print('Instrument prefix ' .. INST_PREFIX)
print('Instrument name: BSVH ' .. INSTR_MAP[INST_PREFIX])
print('Assets directory: ' .. ASSETS_DIR)
print('Samples directory: ' .. SAMPLES_DIR)
print('Utils Directory: ' .. UTILS_DIR)


-- Set correct instrument name
instrument.name = 'B-Side Virtual Horns: ' .. INSTR_MAP[INST_PREFIX]

-- Load instrument KSP script
instrument.scripts[0].name = 'B-Side Virtual Horns ' .. INSTR_MAP[INST_PREFIX]
instrument.scripts[0].linkedFileName = 'bsvh_' .. string.lower(INSTR_MAP[INST_PREFIX])
instrument.scripts[0].linked = true

-- Reset all existing groups
instrument.groups:reset()

local groups = {}
local sample_count = 0

-- Get top-level directories
for _,tld in fs.directory(SAMPLES_DIR) do

    local prefix, scg = string.match(fs.filename(tld), '^(.*)_(.*)')
    
    -- filter TLDs by instrument prefix
    if prefix == INST_PREFIX then
        print('TLD:' .. tld)
        print('SCGroup: ' .. scg)

        -- Get articulation group sub-directories
        for _,agd in fs.directory(tld) do
            local ag = fs.filename(agd)
            print('AGroup: ' .. ag)

            -- Get individual sample files
            for _,sfp in fs.directory(agd) do

                local dyn, t1, t2, rnd
                local f = fs.filename(sfp)

                if ag == 'LEG' then
                    -- legato
                    dyn, t1, t2, rnd = f:match("([^_]+)_([^_]+)_([^_]+)_([^_]+)%.wav")
                    import_sample(scg, ag, dyn, t1, t2, rnd, sfp)
                    sample_count = sample_count + 1
                else
                    -- sustain, short, stacc
                    dyn, t1, rnd = f:match("([^_]+)_([^_]+)_([^_]+)%.wav")
                    import_sample(scg, ag, dyn, t1, t2, rnd, sfp)
                    sample_count = sample_count + 1
                end
            end
        end
    end
end

-- Label Group 0 as 'EMPTY' for reference
instrument.groups[0].name = 'EMPTY'

print("TOTAL NUMBER OF SAMPLES: " .. sample_count)
print("You can now press Push(↑) in order to apply the changes to Kontakt.")