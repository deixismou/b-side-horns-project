#!/usr/bin/env python

import logging
import os
import time
from mido import MidiFile, MidiTrack, Message, MetaMessage, bpm2tempo

ROOT_DIR = os.path.abspath(
    os.path.join(os.path.abspath(__file__), os.pardir, os.pardir))

OUTPUT_DIR = os.path.join(ROOT_DIR, 'Resources', 'guides')

logging.basicConfig(
    filename=os.path.join(ROOT_DIR, 'make_guides.log'),
    encoding='utf-8', level=logging.DEBUG)

NOTE_NAMES_SHARPS = [
    "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"
]

NOTE_NAMES_FLATS = [
    "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"
]

NOTE_MAPPING = {
    "C": 0, "C#": 1, "D": 2, "D#": 3, "E": 4, "F": 5,
    "F#": 6, "G": 7, "G#": 8, "A": 9, "A#": 10, "B": 11,
    "Db": 1, "Eb": 3, "Gb": 6, "Ab": 8, "Bb": 10
}

DYNAMICS_MAPPING = {
    'PPPP': 8, 'PPP': 20, 'PP': 31, 'P': 42, 'MP': 53,
    'MF': 64, 'F': 80, 'FF': 96, 'FFF': 112, 'FFFF': 127
}

VALUES_MAPPING = {

    '1': 4,  # standard
    '2': 2,
    '4': 1,
    '8': 0.5,
    '16': 0.25,
    '32': 0.125,

    '1d': 6,  # dotted
    '2d': 3,
    '4d': 1.5,
    '8d': 0.75,
    '16d': 0.375,
    '32d': 0.1875,

    '2t': 1.3333,  # triplet
    '4t': 0.6666,
    '8t': 0.3333,
    '16t': 0.1666,
    '32t': 0.0833
}

TPQN = 480  # ticks per quarter note
TPBR = 1920  # ticks per bar

BPM60 = bpm2tempo(60, (4, 4))
BPM70 = bpm2tempo(70, (4, 4))
BPM120 = bpm2tempo(120, (4, 4))  # default tempo


def value_to_ticks(value):
    ''' Returns the value in ticks for a note value contained in
        `VALUES_MAPPING`
    '''

    if value not in VALUES_MAPPING:
        raise Exception(f"Invalid note value {value}")

    ticks = TPQN * VALUES_MAPPING[value]

    return int(ticks)


def midi_to_note(midi_note, flats=False):
    ''' Returns the note name for a given midi note number. '''

    if flats:
        note_names = NOTE_NAMES_FLATS
    else:
        note_names = NOTE_NAMES_SHARPS

    # Calculate the octave and note within the octave
    octave = (midi_note // 12) - 1
    note_within_octave = midi_note % 12

    # Get the note name
    note_name = f"{note_names[note_within_octave]}{octave}"

    return note_name


def note_to_midi(note_name):
    ''' Returns the midi note number of a given note name. '''

    # Extract the note and octave from the note name
    note, octave = note_name[:-1], int(note_name[-1])

    # Check if this is a valid note name
    if note not in NOTE_MAPPING:
        raise Exception("Invalid note name provided: {note_name}")

    # Calculate the MIDI note number
    midi_note = (octave + 1) * 12 + NOTE_MAPPING[note]

    return midi_note


def whole_tone_range(minimum, maximum):
    ''' Returns an array of midi note values representing a whole tone
        sequence between two note values.
    '''

    whole_tone_notes = []

    for current_note in range(minimum, maximum + 1, 2):
        whole_tone_notes.append(current_note)

    return whole_tone_notes


def chromatic_range(minimum, maximum):
    ''' Returns an array of midi note values representing a chromatic
        sequence between two note values.
    '''

    chromatic_notes = []

    for current_note in range(minimum, maximum + 1):
        chromatic_notes.append(current_note)

    return chromatic_notes


def diatonic_range(minimum, maximum, key):
    ''' Returns an array of midi note values representing a major
        diatonic sequence between two note values.
    '''

    DIATONIC_SCALE = [0, 2, 4, 5, 7, 9, 11]

    # Get the relative note position of the given key
    key_note = NOTE_MAPPING[key]

    diatonic_notes = []
    current_note = minimum

    while current_note <= maximum:
        # Calculate the relative note position within the key
        note_position = (current_note - key_note) % 12

        # Check if relative note position is within the diatonic scale pattern
        if note_position in DIATONIC_SCALE:
            diatonic_notes.append(current_note)

        # Move to the next note
        current_note += 1

    return diatonic_notes


def create_sustains(name, minimum, maximum, value=TPQN, dynamic=64, rounds=1, tempo=500000):
    ''' Returns a MidiTrack object representing the sustains sequence of the
        midi guide track.
    '''

    root_notes = whole_tone_range(minimum, maximum)
    root_value = value
    rest_value = TPBR - root_value

    sustains = MidiTrack()
    sustains.extend(create_preroll(tempo))
    sustains.append(MetaMessage('marker', text=f"START_{name}", time=0))

    offset = TPBR
    for root in root_notes:

        marker = f"{name}_{midi_to_note(root)}"
        sustains.append(
            MetaMessage('marker', text=marker, time=offset))

        sustains.append(
            Message('note_on', note=root, velocity=dynamic, time=0))
        sustains.append(
            Message('note_off', note=root, time=root_value))

        offset = TPBR * rounds + rest_value

    sustains.append(MetaMessage('marker', text=f"END_{name}", time=offset))

    logging.debug(sustains)

    return sustains


def create_shorts(name, minimum, maximum, dynamic=64, rounds=1, tempo=500000):
    ''' Returns a MidiTrack object representing the shorts sequence of the
        midi guide track.
    '''

    root_notes = whole_tone_range(minimum, maximum)
    root_value = value_to_ticks('8')

    shorts = MidiTrack()
    shorts.extend(create_preroll(tempo))
    shorts.append(MetaMessage('marker', text=f"START_{name}", time=0))

    offset = TPBR
    for root in root_notes:

        marker = f"{name}_{midi_to_note(root)}"
        shorts.append(
            MetaMessage('marker', text=marker, time=offset))

        offset = 0
        for _ in range(4):
            shorts.append(
                Message('note_on', note=root, velocity=dynamic, time=offset))
            shorts.append(
                Message('note_off', note=root, time=root_value))

            # downbeat of next quarter note
            offset = (TPQN - root_value)

        offset += TPBR * rounds

    shorts.append(MetaMessage('marker', text=f"END_{name}", time=offset))

    logging.debug(shorts)

    return shorts


def create_staccatos(name, minimum, maximum, dynamic=64, rounds=1, tempo=500000):
    ''' Returns a MidiTrack object representing the shorts sequence of the
        midi guide track.
    '''

    root_notes = whole_tone_range(minimum, maximum)
    root_value = value_to_ticks('16')

    staccatos = MidiTrack()
    staccatos.extend(create_preroll(tempo))
    staccatos.append(MetaMessage('marker', text=f"START_{name}", time=0))

    offset = TPBR
    for root in root_notes:

        marker = f"{name}_{midi_to_note(root)}"
        staccatos.append(
            MetaMessage('marker', text=marker, time=offset))

        offset = 0
        for _ in range(4):
            staccatos.append(
                Message('note_on', note=root, velocity=dynamic, time=offset))
            staccatos.append(
                Message('note_off', note=root, time=root_value))

            # downbeat of next quarter note
            offset = (TPQN - root_value)

        offset += TPBR * rounds

    staccatos.append(MetaMessage('marker', text=f"END_{name}", time=offset))

    logging.debug(staccatos)

    return staccatos


def create_legatos(name, minimum, maximum, dynamic=64, rounds=1, tempo=500000):
    ''' Returns a MidiTrack object representing the legatos sequence of the
        midi guide track.
    '''

    root_notes = whole_tone_range(minimum, maximum)

    legatos = MidiTrack()
    legatos.extend(create_preroll(tempo))
    legatos.append(MetaMessage('marker', text=f"START_{name}", time=0))

    offset = TPBR
    for root in root_notes:
        for interval in range(1, 13):

            target = root + interval

            if target > maximum:
                break

            marker = f"{name}_{midi_to_note(root)}_{midi_to_note(target)}"
            legatos.append(
                MetaMessage('marker', text=marker, time=offset))

            legatos.append(
                Message('note_on', note=root, velocity=dynamic, time=0))
            legatos.append(
                Message('note_off', note=root, time=TPQN))
            legatos.append(
                Message('note_on', note=target, velocity=dynamic, time=0))
            legatos.append(
                Message('note_off', note=target, time=TPQN))
            legatos.append(
                Message('note_on', note=root, velocity=dynamic, time=0))
            legatos.append(
                Message('note_off', note=root, time=TPQN))

            offset = TPQN + (TPBR * rounds)

    legatos.append(MetaMessage('marker', text=f"END_{name}", time=offset))

    logging.debug(legatos)

    return legatos


def create_grace_notes(name, minimum, maximum, dynamic=64, rounds=1, tempo=500000):
    ''' Returns a MidiTrack object representing the grace note sequence of the
        midi guide track.
    '''

    root_notes = whole_tone_range(minimum, maximum)
    intervals = [-1, -2, 1, 2]

    grace_value = value_to_ticks('16')
    root_value = value_to_ticks('4')
    rest_value = value_to_ticks('4')

    graces = MidiTrack()

    for interval in intervals:

        graces.extend(create_preroll(tempo))
        graces.append(MetaMessage(
            'marker', text=f"START_{name}_{interval}", time=0))

        offset = TPBR
        for root in root_notes:

            target = root + interval
            if target in range(minimum, maximum):

                marker = f"{name}_{midi_to_note(target)}_{midi_to_note(root)}"
                graces.append(
                    MetaMessage('marker', text=marker, time=offset))

                for _ in range(2):
                    offset = rest_value - grace_value
                    graces.append(
                        Message('note_on', note=target, velocity=dynamic, time=offset))
                    graces.append(
                        Message('note_off', note=target, time=grace_value))
                    graces.append(
                        Message('note_on', note=root, velocity=dynamic, time=0))
                    graces.append(
                        Message('note_off', note=root, time=root_value))

                offset = TPBR * rounds

        graces.append(MetaMessage(
            'marker', text=f"END_{name}_{interval}", time=offset))

    logging.debug(graces)

    return graces


def create_doits(name, minimum, maximum, dynamic=64, rounds=1, tempo=500000, ascending=True):
    ''' Returns a MidiTrack object representing the doits/falls sequence of the
        midi guide track.
    '''

    root_notes = whole_tone_range(minimum, maximum)
    target_value = value_to_ticks('16t')
    root_value = value_to_ticks('2d') - (target_value * 3)
    rest_value = value_to_ticks('4')

    doits = MidiTrack()
    doits.extend(create_preroll(tempo))
    doits.append(MetaMessage('marker', text=f"START_{name}", time=0))

    if ascending:
        intervals = [1, 2, 3]
    else:
        intervals = [-1, -2, -3]

    offset = TPBR
    for root in root_notes:

        marker = f"{name}_{midi_to_note(root)}"
        doits.append(
            MetaMessage('marker', text=marker, time=offset))

        offset = int(rest_value)

        doits.append(
            Message('note_on', note=root, velocity=dynamic, time=offset))
        doits.append(
            Message('note_off', note=root, time=root_value))

        for interval in intervals:
            target = root + interval
            doits.append(
                Message('note_on', note=target, velocity=dynamic, time=0))
            doits.append(
                Message('note_off', note=target, time=target_value))

        offset = TPBR * rounds

    doits.append(MetaMessage('marker', text=f"END_{name}", time=offset))

    logging.debug(doits)

    return doits


def create_scoops(name, minimum, maximum, dynamic=64, rounds=1, tempo=500000, ascending=True):
    ''' Returns a MidiTrack object representing the scoops/plops sequence of
        the midi guide track.
    '''

    root_notes = whole_tone_range(minimum, maximum)
    root_value = value_to_ticks('2d')
    target_value = value_to_ticks('16t')
    rest_value = value_to_ticks('4')

    if ascending:
        intervals = [3, 2, 1]
    else:
        intervals = [-3, -2, -1]

    scoops = MidiTrack()
    scoops.extend(create_preroll(tempo))
    scoops.append(MetaMessage('marker', text=f"START_{name}", time=0))

    offset = TPBR
    for root in root_notes:

        marker = f"{name}_{midi_to_note(root)}"
        scoops.append(
            MetaMessage('marker', text=marker, time=offset))

        offset = int(rest_value - (target_value * 3))

        for interval in intervals:
            target = root - interval
            scoops.append(
                Message('note_on', note=target, velocity=dynamic, time=offset))
            scoops.append(
                Message('note_off', note=target, time=target_value))
            offset = 0

        scoops.append(
            Message('note_on', note=root, velocity=dynamic, time=0))
        scoops.append(
            Message('note_off', note=root, time=root_value))

        offset = TPBR * rounds

    scoops.append(MetaMessage('marker', text=f"END_{name}", time=offset))

    logging.debug(scoops)

    return scoops


def create_preroll(default=500000):
    ''' Returns one bar of 60 BPM for easy 2-pop alignment and returns the
        tempo to `default`, which is 120BPM if unspecified.
    '''

    sync = MidiTrack()

    sync.append(MetaMessage('set_tempo', tempo=1000000))
    sync.append(MetaMessage('marker', text='PREROLL'))
    sync.append(MetaMessage('set_tempo', tempo=default, time=TPBR))

    logging.debug(sync)

    return sync


def create_guide_track(minimum, maximum, dynamics, tempo):
    ''' Returns a MidiTrack object representing the complete guide track
        containing all of the guide sequences.
    '''

    guide_track = MidiTrack()

    for dyn in dynamics:
        guide_track.extend(
            create_sustains(
                f"SUS_{dyn}",
                minimum,
                maximum,
                value_to_ticks('2d'),
                DYNAMICS_MAPPING[dyn],
                4,
                tempo
            )
        )

    for dyn in dynamics:
        guide_track.extend(
            create_shorts(
                f"SRT_{dyn}",
                minimum,
                maximum,
                DYNAMICS_MAPPING[dyn],
                1,
                tempo
            )
        )

    for dyn in dynamics:
        guide_track.extend(
            create_staccatos(
                f"STA_{dyn}",
                minimum,
                maximum,
                DYNAMICS_MAPPING[dyn],
                1,
                tempo
            )
        )

    for dyn in dynamics:
        guide_track.extend(
            create_legatos(
                f"LEG_{dyn}",
                minimum,
                maximum,
                DYNAMICS_MAPPING[dyn],
                2,
                tempo
            )
        )

    # for dyn in dynamics:
    #     guide_track.extend(
    #         create_grace_notes(
    #             f"GRA_{dyn}",
    #             minimum,
    #             maximum,
    #             DYNAMICS_MAPPING[dyn],
    #             2,
    #             tempo
    #         )
    #     )

    # for dyn in dynamics:
    #     guide_track.extend(
    #         create_sustains(
    #             f"DOI_{dyn}",
    #             minimum,
    #             maximum,
    #             value_to_ticks('2'),
    #             DYNAMICS_MAPPING[dyn],
    #             2,
    #             tempo,
    #         )
    #     )

    # for dyn in dynamics:
    #     guide_track.extend(
    #         create_sustains(
    #             f"FAL_{dyn}",
    #             minimum,
    #             maximum,
    #             value_to_ticks('2'),
    #             DYNAMICS_MAPPING[dyn],
    #             2,
    #             tempo,
    #         )
    #     )

    # for dyn in dynamics:
    #     guide_track.extend(
    #         create_sustains(
    #             f"SCP_{dyn}",
    #             minimum,
    #             maximum,
    #             value_to_ticks('2'),
    #             DYNAMICS_MAPPING[dyn],
    #             2,
    #             tempo,
    #         )
    #     )

    # for dyn in dynamics:
    #     guide_track.extend(
    #         create_sustains(
    #             f"PLP_{dyn}",
    #             minimum,
    #             maximum,
    #             value_to_ticks('2'),
    #             DYNAMICS_MAPPING[dyn],
    #             2,
    #             tempo,
    #         )
    #     )

    # for dyn in dynamics:
    #     guide_track.extend(
    #         create_sustains(
    #             f"GWL_{dyn}",
    #             minimum,
    #             maximum,
    #             value_to_ticks('2d'),
    #             DYNAMICS_MAPPING[dyn],
    #             2,
    #             tempo
    #         )
    #     )

    guide_track.append(MetaMessage('marker', text=f"END_OF_SESSION", time=0))
    guide_track.append(MetaMessage('end_of_track'))

    return guide_track


def main():

    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)

    start_time = time.time()

    dynamics = ['MF', 'F', 'FF']
    tempo = 857143  # 70 BPM

    # PROTOTYPE ( PTYP )
    # note_min = 'G2'
    # note_max = 'G3'
    # midi_min = note_to_midi(note_min)
    # midi_max = note_to_midi(note_max)

    # ptyp_guide = MidiFile()
    # ptyp_guide.tracks.append(
    #     create_guide_track(midi_min, midi_max, dynamics, tempo))
    # ptyp_guide.save(
    #     os.path.join(OUTPUT_DIR, f'PTYP_{note_min}_{note_max}.mid'))

    # # TROMBONE ( TRBN )
    note_min = 'E2'
    note_max = 'F5'
    midi_min = note_to_midi(note_min)
    midi_max = note_to_midi(note_max)

    logging.info(f"Begin TROMBONE guides: {note_min} - {note_max}")

    trbn_guide = MidiFile()
    trbn_guide.tracks.append(
        create_guide_track(midi_min, midi_max, dynamics, tempo))
    trbn_guide.save(
        os.path.join(OUTPUT_DIR, f'TRBN_{note_min}_{note_max}.mid'))

    # # BARITONE SAX ( BSAX )
    note_min = 'C#2'
    note_max = 'G#4'
    midi_min = note_to_midi(note_min)
    midi_max = note_to_midi(note_max)

    logging.info(f"Begin BARITONE SAX guides: {note_min} - {note_max}")

    bsax_guide = MidiFile()
    bsax_guide.tracks.append(
        create_guide_track(midi_min, midi_max, dynamics, tempo))
    bsax_guide.save(
        os.path.join(OUTPUT_DIR, f'BSAX_{note_min}_{note_max}.mid'))

    # # TENOR SAX ( TSAX )
    note_min = 'Ab2'
    note_max = 'E5'
    midi_min = note_to_midi(note_min)
    midi_max = note_to_midi(note_max)

    logging.info(f"Begin TENOR SAX guides: {note_min} - {note_max}")

    tsax_guide = MidiFile()
    tsax_guide.tracks.append(
        create_guide_track(midi_min, midi_max, dynamics, tempo))
    tsax_guide.save(
        os.path.join(OUTPUT_DIR, f'TSAX_{note_min}_{note_max}.mid'))

    # # TRUMPET ( TRPT )
    note_min = 'F#3'
    note_max = 'E6'
    midi_min = note_to_midi(note_min)
    midi_max = note_to_midi(note_max)

    logging.info(f"Begin TRUMPET guides: {note_min} - {note_max}")

    trpt_guide = MidiFile()
    trpt_guide.tracks.append(
        create_guide_track(midi_min, midi_max, dynamics, tempo))
    trpt_guide.save(
        os.path.join(OUTPUT_DIR, f'TRPT_{note_min}_{note_max}.mid'))

    logging.info("Script completed in %0.3f seconds" %
                 (time.time() - start_time))


if __name__ == '__main__':
    main()
