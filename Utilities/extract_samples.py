#!/usr/bin/env python

import datetime
import logging
import os
import sox

from mido import MidiFile
from mido import bpm2tempo, tick2second


ROOT_DIR = os.path.abspath(
    os.path.join(os.path.abspath(__file__), os.pardir, os.pardir))

INPUT_DIR = os.path.join(ROOT_DIR, 'Resources', 'bounces')
OUTPUT_DIR = os.path.join(ROOT_DIR, 'Samples')
MIDI_DIR = os.path.join(ROOT_DIR, 'Resources', 'guides')

logging.basicConfig(
    filename=os.path.join(ROOT_DIR, 'extract_samples.log'),
    encoding='utf-8', level=logging.DEBUG)

TPQN = 480  # ticks per quarter note
TPBR = 1920  # ticks per bar

# duration (tks), pre_pad (s), post_pad (s)
REL_MAP = (240, 0.05, 0.2)

""" prefix, duration (tks), pre_pad (s), post_pad (s), bar_ratio, rounds
    NOTE: post_pad must compensate for pre_pad, since sample length is
    calculated from pre_pad start position.
"""
SAMPLE_MAP = [
    ('SUS', 1440, 0.02, 0.2, 1, 4),                 # len 1 bar
    ('SRT', 240, 0.02, 0.2, 0.25, 4),                # len 1/16
    ('STA', 240, 0.02, 0.2, 0.25, 4),               # len 1/16
#    ('LEG', 120, -0.64285725, 0.4285715, 1, 2),     # len 1/8; pre-pad -1/8th
#    ('GRA', 600, 0.00, 0.2, 0.5, 2),
#    ('DOI', 1440, 0.05, 0.2, 1, 2),
#    ('FAL', 1440, 0.05, 0.2, 1, 2),
#    ('SCP', 1440, 0.05, 0.2, 1, 2),
#    ('PLP', 1440, 0.05, 0.2, 1, 2),
#    ('GWL', 1440, 0.05, 0.2, 1, 2),
]


def enumerateSamples(mfile: MidiFile):
    """ Generates a list of absolute start and end cues for each sample in a
        midi guide track.
    """

    # Defaults
    tempo = bpm2tempo(120, (4, 4))

    samples = []

    t = 0
    for msg in mfile.tracks[0]:

        t += tick2second(msg.time, TPQN, tempo)
        s_map = ()

        # if msg is a tempo change, update tempo
        if msg.type == 'set_tempo':
            tempo = msg.tempo

        # if msg is a sample marker, get its s_map
        if msg.type == 'marker':
            for i in SAMPLE_MAP:
                if msg.text.split('_')[0] in i:
                    s_map = i

        # if msg isn't a sample marker, next message
        if len(s_map) == 0:
            continue

        # This line is only needed for parsing prototype guide tracks
        if len(s_map) > 0 and msg.text.split('_')[2] in list({'START', 'END'}):
            continue

        # for the sake of readability
        prefix, len_t, pre_pad, post_pad, ratio, rounds = s_map
        len_s = tick2second(len_t, TPQN, tempo)

        for r in range(rounds):

            # adjust for sample position relative to marker
            SPBR = tick2second(TPBR, TPQN, tempo)
            offset = tick2second(r * (TPBR * ratio), TPQN, tempo)

            label = f"{msg.text}_{r+1:03}"

            # Calculate start and end points
            start = t + SPBR + offset - pre_pad
            end = start + len_s + post_pad

            logSample(label, start, end)

            # add sample to list
            samples.append(
                (label, start, end)
            )

            if prefix == 'SUS':

                # Get release sample attributes
                rlen_t, pre_pad, post_pad = REL_MAP
                rlen_s = tick2second(rlen_t, TPQN, tempo)

                # Create release label
                text = msg.text.split('_')
                label = f"REL_{text[1]}_{text[2]}_{r+1:03}"

                # Calculate release sample (end of the note)
                start = t + SPBR + offset + len_s - pre_pad
                end = start + rlen_s + post_pad

                logSample(label, start, end)

                # add sample to list
                samples.append(
                    (label, start, end)
                )

            if prefix == 'LEG':

                # Calculate descending pair (advance one quarter note)
                start += tick2second(TPQN, TPQN, tempo)
                end += tick2second(TPQN, TPQN, tempo)

                # Reorganise label for descending targets
                text = msg.text.split('_')
                label = f"{text[0]}_{text[1]}_{text[3]}_{text[2]}_{r+1:03}"

                logSample(label, start, end)

                # add sample to list
                samples.append(
                    (label, start, end)
                )

    return samples


def extractSamples(infile: str, outdir: str, samples: list):
    """ Takes an input file and an enumerated list of start and end cues, then
        extracts each cue from the source flie as an isolated sample clip.
    """

    for sample in samples:

        outfile = os.path.join(outdir, f'{sample[0]}.wav')

        logging.debug(
            f'EXTRACTING: {outfile}')

        tfm = sox.Transformer()
        tfm.trim(sample[-2], sample[-1])
        tfm.fade(0.010, 0.010, 'l')
        tfm.build_file(infile, outfile)


def logSample(label, start, end):

    start_time = datetime.timedelta(seconds=start)
    end_time = datetime.timedelta(seconds=end)
    logging.debug(f"ENUMERATING: {label}  {start_time}  {end_time}")


def main():

    input_files = os.listdir(INPUT_DIR)
    midi_files = os.listdir(MIDI_DIR)

    for midi_file in midi_files:

        # check that this is a midi file
        if midi_file.split('.')[-1] != 'mid':
            continue

        mfile = MidiFile(filename=os.path.join(MIDI_DIR, midi_file))
        m_prefix = midi_file.split('.')[0]

        logging.debug(f"MIDI FILE FOUND: {midi_file}")

        samples = enumerateSamples(mfile)

        sample_groups = {}
        for sample in samples:

            label = sample[0].split('_')[0]
            sample_groups.setdefault(label, [])
            sample_groups[label].append(sample)

        for input_file in input_files:

            infile = os.path.join(INPUT_DIR, input_file)
            in_prefix = input_file.split('.')[0]

            # input file does not match this midi file
            if in_prefix.split('_')[0] != m_prefix.split('_')[0]:
                logging.debug(f"AUDIO FILE SKIPPED: {input_file}")
                continue

            logging.debug(f"AUDIO FILE MATCHED: {input_file}")

            for group in sample_groups:

                outdir = os.path.join(OUTPUT_DIR, in_prefix, group)
                if not os.path.exists(outdir):
                    os.makedirs(outdir)

                logging.debug(f"READING: {infile}")
                extractSamples(infile, outdir, sample_groups[group])


if __name__ == '__main__':
    main()
