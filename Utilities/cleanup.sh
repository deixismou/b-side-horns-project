#!/usr/bin/env bash

start_dir=$(pwd)
root_dir=/Users/coreybresnan/Development/b-side-horns-project

cd "$root_dir"

for f in ./Samples/*REL; do

	# Remove after copying the contents from REL extraction pass

	release_dir="$f/REL"
	target_dir=${f%-REL}

	rm -r "$target_dir/REL"
	cp -r "$release_dir" "$target_dir"
	rm -r "$f"

done

# Remove empty samples from PTYP session
for f in ./Samples/PTYP*; do

	# Remove unwanted articulations
	exclude_folders=('DOI' 'FAL' 'GRA' 'GWL' 'PLP' 'SCP')
	
	for folder in ${exclude_folders[@]}; do
	 	find $f -type d -name "$folder" -exec rm -rf {} +
	 done

	# Remove unwanted round robins
	find $f -type f -name 'REL*002.wav' -exec rm -f {} +
	find $f -type f -name 'REL*003.wav' -exec rm -f {} +
	find $f -type f -name 'REL*004.wav' -exec rm -f {} +
	find $f -type f -name 'LEG*002.wav' -exec rm -f {} +
	find $f -type f -name 'SRT*003.wav' -exec rm -f {} +
	find $f -type f -name 'SRT*004.wav' -exec rm -f {} +
	find $f -type f -name 'SUS*002.wav' -exec rm -f {} +

done

# Remove empty samples from PTYP session
	for f in ./Samples/TRBN*; do

		# Remove unwanted articulations
		exclude_folders=('REL' 'DOI' 'FAL' 'GRA' 'GWL' 'PLP' 'SCP')

		for folder in ${exclude_folders[@]}; do
		 	find $f -type d -name "$folder" -exec rm -rf {} +
		 done

		# Remove unwanted round robins
		find $f -type f -name 'LEG*002.wav' -exec rm -f {} +
		find $f -type f -name 'REL*002.wav' -exec rm -f {} +
		find $f -type f -name 'REL*003.wav' -exec rm -f {} +
		find $f -type f -name 'REL*004.wav' -exec rm -f {} +

		exclude_notes=('D4' 'E4' 'F#4' 'G#4' 'A#4' 'C5' 'D5' 'E5')

		for note in ${exclude_notes[@]}; do
	  		find $f -type f -name "*${note}*.wav" -exec rm {} +
		done
	done

# Remove empty samples from PTYP session
for f in ./Samples/BSAX*; do

	# Remove unwanted articulations
	exclude_folders=('REL' 'DOI' 'FAL' 'GRA' 'GWL' 'PLP' 'SCP')
	
	for folder in ${exclude_folders[@]}; do
	 	find $f -type d -name "$folder" -exec rm -rf {} +
	 done

	# Remove unwanted round robins
	find $f -type f -name 'LEG*002.wav' -exec rm -f {} +
	find $f -type f -name 'REL*002.wav' -exec rm -f {} +
	find $f -type f -name 'REL*003.wav' -exec rm -f {} +
	find $f -type f -name 'REL*004.wav' -exec rm -f {} +

	mv "${f}-REL/REL" "${f}/REL"

done

# Remove empty samples from PTYP session
for f in ./Samples/TSAX*; do

	# Remove unwanted articulations
	exclude_folders=('REL' 'DOI' 'FAL' 'GRA' 'GWL' 'PLP' 'SCP')
	
	for folder in ${exclude_folders[@]}; do
	 	find $f -type d -name "$folder" -exec rm -rf {} +
	 done

	# Remove unwanted round robins
	find $f -type f -name 'LEG*002.wav' -exec rm -f {} +
	find $f -type f -name 'REL*002.wav' -exec rm -f {} +
	find $f -type f -name 'REL*003.wav' -exec rm -f {} +
	find $f -type f -name 'REL*004.wav' -exec rm -f {} +

done

# Remove empty samples from PTYP session
for f in ./Samples/TRPT*; do

	# Remove unwanted articulations
	exclude_folders=('REL' 'DOI' 'FAL' 'GRA' 'GWL' 'PLP' 'SCP')
	
	for folder in ${exclude_folders[@]}; do
	 	find $f -type d -name "$folder" -exec rm -rf {} +
	 done

	# Remove unwanted round robins
	find $f -type f -name 'LEG*002.wav' -exec rm -f {} +
	find $f -type f -name 'REL*002.wav' -exec rm -f {} +
	find $f -type f -name 'REL*003.wav' -exec rm -f {} +
	find $f -type f -name 'REL*004.wav' -exec rm -f {} +

done

# rm -r ./Samples/*REL

# Remove log files
find . -type f -name '*.log' -exec rm -f {} +

# Remove .DS_Store files
find . -type f -name '.DS_Store' -exec rm -f {} +

cd "$start_dir"
