{
*****************   B-Side Virtual Horns Project    ****************************

This file is part of the B-Side Virtual Horns Project - A sample-based virtual 
instrument, written for Kontakt.

Copyright (c) 2024 Corey Mark Bresnan

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

---

CONTACTS

Corey Mark Bresnan
E: 1021657@student.sae.edu.au
IG: @coreymarkbresnan

}

{#pragma save_compiled_source ../Resources/scripts/bsvh_trombone.txt}

import "bsvh_main.ksp"

macro Instrument.init

	family Instrument

		declare @name := 'BSVH: Trombone'
		declare @wallpaper := 'wallpaper_trbn'
	
		declare min_range := 40 // E2
		declare max_range := 60 // C4

	end family

end macro