<div align="center">
![B-Side Virtual Horns Project"](Documentation/assets/header.png "B-Side Virtual Horns Project")
</div>

## About the project

This repository contains Kontakt instrument files and automation scripts for the _B-Side Virtual Horns Project_.

This project is a student work created as part of the SAE Institute's Bachelor Programme: _CIM312 Major Project Development_ and _CIM330 Major Project Production_.

Our goal is to create a sample-based virtual instrument plugin which is inspired by the iconic brass sections featured in the soul and R&B recordings of the 1960s. These ensembles, which typically feature trumpets, tenor saxophone, baritone saxophone and trombone, are an essential part of the rhythm sections used in Motown, FAME, Muscle Shoals and Stax recordings from the era. 

Using a combination of instrument sampling, digital and analog recording techniques, we hope to capture the characteristic sound of these recordings in a sample instrument.

## Instrument Files & Samples

This instrument was developed for Native Instruments Kontakt, therefore you will need to have a fully licensed version of Kontakt installed (version 6 or later). The instrument will work perfectly well in the free "player" version of Kontakt, however it will run in _demo mode_ only, and will stop playing audio after 15 minutes.

The code and assets in this repository are required to build the instrument from scratch, and are primarily used for development. If you simply wish to download and use the instrument in Kontakt, you need only [download the latest audio samples and instrument files here](https://drive.google.com/drive/folders/1ws0N7cEJQPkz-i1ybs8gWAHa1RXkHp-1?usp=drive_link).


<div align="center">
![B-Side Virtual Horns Project"](Documentation/assets/instrument_decals.png "B-Side Virtual Horns Project")
</div>

## Acknowledgements

The creators of this instrument would like to acknowledge the following projects, which were highly useful to us in the development process:

* David Healey’s excellent [tutorial series on Kontakt scripting](https://xtant-audio.com/product/kontakt-scripting-collection/).
* The [SublimeKSP precompiler](https://github.com/nojanath/SublimeKSP?tab=readme-ov-file) and editor plugin, which was created by Nils Liberg, and later forked by Jonathan Thompson.
* The [Koala advanced scripting library](https://github.com/magneto538/Koala) for Kontakt KSP, which was developed by Davide Magni.

## Contributors

This project involved the creative efforts of seven people, roughly 40 hours of recording studio sessions, and countless hours of editing, scripting, designing and testing. For many of the project contributors, this represents their first ever attempt at building a complex sample instrument. It was deisigned and built over 26 weeks of planning and development.

* _Project Lead:_ Corey Mark Bresnan
* _Audio Engineer:_ Noah Slattery
* _Graphic Designer:_ Thais Machado Pereira

* _Trumpet:_ Paul Murchison
* _Tenor Sax:_ Bootsie Booth
* _Baritone Sax:_ Martin Jucker
* _Trombone:_ Amy McCarthy

* _Academic Supervisors:_ Jack Williams, Duncan Maclean

## Licenses

The instrument build files and utilities contained within this repository are realeased under MIT License, and the audio samples which accompany them are realeased under Creative Commons CC-BY 4.0. We encorage the adaptation and further development of our efforts in your own creative work, provided the creators above are adequately acknowledged and credited for their work. We would also love to hear what you made with our instrument, so get in touch and drop us a link.